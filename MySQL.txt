CREATE TABLE `food`.`admin` (
  `id` VARCHAR(20) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `restaurantID` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `restaurantID_idx` (`restaurantID` ASC) VISIBLE,
  CONSTRAINT `restaurantID`
    FOREIGN KEY (`restaurantID`)
    REFERENCES `food`.`restaurant` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE `food`.`allorders` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `userID` INT NOT NULL,
  `sum` FLOAT NOT NULL,
  `date` DATETIME NOT NULL,
  `status` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  INDEX `userID_idx` (`userID` ASC) VISIBLE,
  CONSTRAINT `userID`
    FOREIGN KEY (`userID`)
    REFERENCES `food`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE `food`.`restaurant` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `address` VARCHAR(45) NULL,
  `telephone` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE);

CREATE TABLE `food`.`food` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `price` FLOAT NOT NULL,
  `description` VARCHAR(45) NULL,
  `menuID` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  INDEX `menuID_idx` (`menuID` ASC) VISIBLE,
  CONSTRAINT `menuID`
    FOREIGN KEY (`menuID`)
    REFERENCES `food`.`menu` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE `food`.`orderr` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `foodID` INT NOT NULL,
  `allOrdersID` INT NOT NULL,
  `buc` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  INDEX `foodID_idx` (`foodID` ASC) VISIBLE,
  CONSTRAINT `foodID`
    FOREIGN KEY (`foodID`)
    REFERENCES `food`.`food` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE `food`.`menu` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` INT NOT NULL,
  `restaurantID` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  INDEX `restaurantID_idx` (`restaurantID` ASC) VISIBLE,
  CONSTRAINT `restaurantID`
    FOREIGN KEY (`restaurantID`)
    REFERENCES `food`.`restaurant` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE `food`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` INT NOT NULL,
  `email` INT NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `address` VARCHAR(45) NULL DEFAULT NULL,
  `postcode` VARCHAR(45) NULL DEFAULT NULL,
  `city` VARCHAR(45) NULL DEFAULT NULL,
  `country` VARCHAR(45) NULL DEFAULT NULL,
  `telephone` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE);








