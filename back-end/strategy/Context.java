package spring.demo.strategy;

import spring.demo.dto.BillDTO;

import java.util.List;

public class Context {
    private Strategy strategy;

    public Context(Strategy strategy){
        this.strategy = strategy;
    }

    public List<BillDTO> executeStrategy(){
        return strategy.showOrders();
    }
}
