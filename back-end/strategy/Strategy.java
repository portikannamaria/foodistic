package spring.demo.strategy;

import spring.demo.dto.BillDTO;
import spring.demo.dto.RestaurantDTO;

import java.util.List;

public interface Strategy {
    public List<BillDTO> showOrders();
}
