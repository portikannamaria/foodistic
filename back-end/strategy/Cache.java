package spring.demo.strategy;

import spring.demo.dto.BillDTO;

import java.util.ArrayList;
import java.util.List;

public class Cache implements Strategy{

    private static Cache cache_instance=null;
    public List<BillDTO> orderDTOS;

    private Cache(){
        orderDTOS=new ArrayList<>();
    }

    @Override
    public List<BillDTO> showOrders(){
        return orderDTOS;
    }

    public static Cache getInstance()
    {
        if (cache_instance == null)
            cache_instance = new Cache();

        return cache_instance;
    }
}
