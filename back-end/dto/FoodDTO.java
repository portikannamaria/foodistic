package spring.demo.dto;

public class FoodDTO {
    private Integer id;
    private String name;
    private Float price;
    private String description;
    private Integer menuID;

    public FoodDTO() {
    }

    public FoodDTO(Integer id, String name, Float price, String description,Integer menuID) {
        super();
        this.id = id;
        this.name=name;
        this.price=price;
        this.description=description;
        this.menuID=menuID;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getPrice() {
        return this.price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getMenuID() {
        return this.menuID;
    }

    public void setMenuID(Integer menuID) {
        this.menuID = menuID;
    }




    public static class Builder {
        private Integer nestedid;
        private String nestedname;
        private Float nestedprice;
        private String nesteddescription;
        private Integer nestedmenuid;

        public Builder id(int id) {
            this.nestedid = id;
            return this;
        }

        public Builder name(String name) {
            this.nestedname = name;
            return this;
        }

        public Builder price(float price) {
            this.nestedprice = price;
            return this;
        }

        public Builder description(String description) {
            this.nesteddescription = description;
            return this;
        }

        public Builder menuID(int menuID) {
            this.nestedmenuid = menuID;
            return this;
        }



        public FoodDTO create() {
            return new FoodDTO(nestedid, nestedname,nestedprice,nesteddescription,nestedmenuid);
        }

    }
}
