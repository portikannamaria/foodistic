package spring.demo.dto;

public class OrderDTO {
    private Integer id;
    private Integer userID;
    private Integer foodID;
    private Integer allOrdersID;
    private Integer buc;

    public OrderDTO() {
    }

    public OrderDTO(Integer id, Integer foodID, Integer allOrdersID,Integer buc) {
        super();
       // this.userID=userID;
        this.id = id;
        this.foodID=foodID;
        this.allOrdersID=allOrdersID;
        this.buc=buc;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFoodID() {
        return foodID;
    }

    public void setFoodID(Integer foodID) {
        this.foodID = foodID;
    }

    public Integer getAllOrdersID() {
        return allOrdersID;
    }

    public void setAllOrdersID(Integer allOrdersID) {
        this.allOrdersID = allOrdersID;
    }

    public Integer getBuc() {
        return this.buc;
    }

    public void setBuc(Integer buc) {
        this.buc = buc;
    }




    public static class Builder {
        private Integer nestedid;
        private Integer nestedfoodid;
        private Integer nestedallordersid;
        private Integer nestedbuc;

        public Builder id(int id) {
            this.nestedid = id;
            return this;
        }

        public Builder foodID(int foodID) {
            this.nestedfoodid = foodID;
            return this;
        }

        public Builder allOrdersID(int allOrdersID) {
            this.nestedallordersid = allOrdersID;
            return this;
        }

        public Builder buc(int buc) {
            this.nestedbuc = buc;
            return this;
        }



        public OrderDTO create() {
            return new OrderDTO(nestedid, nestedfoodid,nestedallordersid,nestedbuc);
        }

    }
}
