package spring.demo.dto;

import spring.demo.strategy.Strategy;

import java.util.Date;

public class FullOrderDTO {
    private Integer id;
    private Integer userID;
    private Float sum;
    private String date;
    private String status;
    private String restaurant;

    public FullOrderDTO() {
    }

    public FullOrderDTO(Integer id, Integer userID, Float sum, String date, String status, String restaurant) {
        super();
        this.id = id;
        this.userID=userID;
        this.sum=sum;
        this.date=date;
        this.status=status;
        this.restaurant=restaurant;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserID() {
        return this.userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public Float getSum() {
        return this.sum;
    }

    public void setSum(Float sum) {
        this.sum = sum;
    }

    public String getDate() {
        return this.date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setRestaurant(String restaurant) {
        this.restaurant = restaurant;
    }

    public String getRestaurant() {
        return this.restaurant;
    }


    public static class Builder {
        private Integer nestedid;
        private Integer nesteduserid;
        private Float nestedsum;
        private String nesteddate;
        private String nestedstatus;
        private String nestedrest;

        public Builder id(int id) {
            this.nestedid = id;
            return this;
        }

        public Builder userID(int userid) {
            this.nesteduserid = userid;
            return this;
        }

        public Builder sum(float sum) {
            this.nestedsum = sum;
            return this;
        }

        public Builder date(Date date) {
            this.nesteddate = date.toString();
            return this;
        }

        public Builder status(String status) {
            this.nestedstatus = status;
            return this;
        }

        public Builder restaurant(String rest)
        {
            this.nestedrest=rest;
            return this;
        }


        public FullOrderDTO create() {
            return new FullOrderDTO(nestedid, nesteduserid,nestedsum,nesteddate,nestedstatus,nestedrest);
        }

    }
}
