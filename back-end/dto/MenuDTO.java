package spring.demo.dto;

public class MenuDTO {
    private Integer id;
    private String name;
    private Integer restaurantID;

    public MenuDTO() {
    }

    public MenuDTO(Integer id, String name, Integer restaurantID) {
        super();
        this.id = id;
        this.name=name;
        this.restaurantID=restaurantID;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getRestaurantID() {
        return restaurantID;
    }

    public void setRestaurantID(Integer restaurantID) {
        this.restaurantID = restaurantID;
    }





    public static class Builder {
        private Integer nestedid;
        private Integer nestedrestaurantid;
        private String nestedname;

        public Builder id(int id) {
            this.nestedid = id;
            return this;
        }

        public Builder name(String name) {
            this.nestedname = name;
            return this;
        }

        public Builder restaurantID(int restaurantID) {
            this.nestedrestaurantid = restaurantID;
            return this;
        }



        public MenuDTO create() {
            return new MenuDTO(nestedid,nestedname,nestedrestaurantid);
        }

    }
}
