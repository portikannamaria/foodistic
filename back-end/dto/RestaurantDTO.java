package spring.demo.dto;

public class RestaurantDTO {
    private Integer id;
    private String name;
    private String address;
    private String telephone;

    public RestaurantDTO() {
    }

    public RestaurantDTO(Integer id, String name, String address, String telephone) {
        super();
        this.id = id;
        this.name= name;
        this.address = address;
        this.telephone = telephone;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }





    public static class Builder {
        private Integer nestedid;
        private String nestedname;
        private String nestedaddress;
        private String nestedtelephone;

        public Builder id(int id) {
            this.nestedid = id;
            return this;
        }

        public Builder name(String name) {
            this.nestedname = name;
            return this;
        }

        public Builder address(String address) {
            this.nestedaddress = address;
            return this;
        }

        public Builder telephone(String telephone) {
            this.nestedtelephone = telephone;
            return this;
        }



        public RestaurantDTO create() {
            return new RestaurantDTO(nestedid, nestedname,nestedaddress,nestedtelephone);
        }

    }
}
