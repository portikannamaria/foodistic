package spring.demo.dto;

import java.util.Date;

public class BillDTO {
    private String food;
    private Float price;
    private Integer buc;

    public BillDTO() {
    }

    public BillDTO(String food, Float price, Integer buc) {
        super();
        this.food=food;
        this.price=price;
        this.buc=buc;
    }


    public String getFood() {
        return food;
    }

    public void setFood(String food) {
        this.food = food;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Integer getBuc() {
        return this.buc;
    }

    public void setBuc(Integer buc) {
        this.buc = buc;
    }


    public static class Builder {
        private String nestedfood;
        private Float nestedprice;
        private Integer nestedbuc;

        public Builder food(String food) {
            this.nestedfood = food;
            return this;
        }

        public Builder price(float price) {
            this.nestedprice = price;
            return this;
        }

        public Builder buc(int buc) {
            this.nestedbuc = buc;
            return this;
        }


        public BillDTO create() {
            return new BillDTO(nestedfood, nestedprice,nestedbuc);
        }

    }
}
