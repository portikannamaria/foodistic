package spring.demo.entities;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "user")
public class User extends Observer implements java.io.Serializable,Person {

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String name;
	private String email;
	private String password;
	private String address;
	private String postcode;
	private String city;
	private String country;
	private String telephone;
	private String message;
	private FullOrder fullOrder;

	public User() {
	}

	public User(Integer id, String name, String email, String password, String address, String postcode, String city, String country,
			String telephone) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.password=password;
		this.address = address;
		this.postcode = postcode;
		this.city = city;
		this.country = country;
		this.telephone = telephone;
	}
	@Override
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}
	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "email", unique = true, nullable = false, length = 200)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	@Override
	@Column(name = "password",nullable = false, length = 45)
	public String getPassword() {
		return this.password;
	}
	@Override
	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "address", nullable = true, length = 200)
	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name = "postcode", nullable = true, length = 50)
	public String getPostcode() {
		return this.postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	@Column(name = "city", nullable = true, length = 100)
	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Column(name = "country", nullable = true, columnDefinition = "char(2)")
	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Column(name = "telephone", nullable = true, length = 50)
	public String getTelephone() {
		return this.telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	@Override
	@Column(name = "name", nullable = false, length = 100)
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void update(String message) {
		this.message=message;
	}

	public void attach(FullOrder fullOrder){
		this.fullOrder=fullOrder;
		this.fullOrder.attach(this);
	}
	public String returnMessage(){
		return this.message;
	}
}
