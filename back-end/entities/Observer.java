package spring.demo.entities;

public abstract class Observer {
    protected FullOrder fullOrder;
    public abstract void update(String message);
}
