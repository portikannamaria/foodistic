package spring.demo.entities;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;


@Entity
@Table(name = "allorders")
public class FullOrder implements java.io.Serializable{
    private static final long serialVersionUID = 1L;
    private Observer observer;
    private Integer id;
    private Integer userID;
    private Float sum;
    private Date date;
    private String status;

    public FullOrder() {
    }

    public FullOrder(Integer id, Integer userID,Float sum,Date date,String status) {
        super();
        this.id = id;
        this.userID=userID;
        this.sum=sum;
        this.date=date;
        this.status=status;
    }


    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "userID", nullable = false, length = 200)
    public Integer getUserID() {
        return this.userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    @Column(name = "sum", nullable = false, length = 200)
    public Float getSum() {
        return this.sum;
    }

    public void setSum(Float sum) {
        this.sum = sum;
    }


    @Column(name = "date", nullable = false, length = 200)
    public Date getDate() {
        return this.date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Column(name = "status", nullable = true, length = 200)
    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
        //notifyObserver("Order with id "+this.id+": "+status);
    }

    public void attach(Observer observer){
       this.observer=observer;
    }

    public void notifyObserver(String message){
            observer.update(message);
    }
}
