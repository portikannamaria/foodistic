package spring.demo.entities;

public interface Person {
    public Integer getId();

    public void setId(Integer id);

    public String getName();

    public void setName(String name);

    public String getPassword();

    public void setPassword(String password);
}
