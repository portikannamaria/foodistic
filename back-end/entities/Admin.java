package spring.demo.entities;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "admin")
public class Admin implements java.io.Serializable,Person {

    private static final long serialVersionUID = 1L;
    private Integer id;
    private String name;
    private String password;
    private Integer restaurantID;

    public Admin() {
    }

    public Admin(Integer id, String name, String password, Integer restaurantID) {
        super();
        this.id = id;
        this.name=name;
        this.password=password;
        this.restaurantID=restaurantID;
    }
    @Override
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    @Column(name = "name",nullable = false, length = 45)
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    @Column(name = "password",nullable = false, length = 45)
    public String getPassword() {
        return this.password;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "restaurantID", nullable = false)
    public Integer getRestaurantID() {
        return this.restaurantID;
    }

    public void setRestaurantID(Integer id) {
        this.restaurantID=id;
    }


}
