package spring.demo.entities;


import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.criteria.CriteriaBuilder;


@Entity
@Table(name = "orderr")
public class Order implements java.io.Serializable {
    private static final long serialVersionUID = 1L;
    private Integer id;
    private Integer foodID;
    private Integer allOrdersID;
    private Integer buc;

    public Order() {
    }

    public Order(Integer id, Integer foodID, Integer allOrdersID, Integer buc) {
        super();
        this.id = id;
        this.foodID=foodID;
        this.allOrdersID=allOrdersID;
        this.buc=buc;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "foodID", nullable = false, length = 200)
    public Integer getFoodID() {
        return this.foodID;
    }

    public void setFoodID(Integer foodID) {
        this.foodID = foodID;
    }

    @Column(name = "allOrdersID", nullable = false, length = 200)
    public Integer getAllOrdersID() {
        return this.allOrdersID;
    }

    public void setAllOrdersID(Integer allOrdersID) {
        this.allOrdersID = allOrdersID;
    }


    @Column(name = "buc", nullable = false, length = 200)
    public Integer getBuc() {
        return this.buc;
    }

    public void setBuc(Integer buc) {
        this.buc = buc;
    }

}
