package spring.demo.entities;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "food")
public class Food implements java.io.Serializable{
    private static final long serialVersionUID = 1L;
    private Integer id;
    private String name;
    private Float price;
    private String description;
    private Integer menuID;

    public Food() {
    }

    public Food(Integer id, String name, Float price, String description,Integer menuID) {
        super();
        this.id = id;
        this.name=name;
        this.price=price;
        this.description=description;
        this.menuID=menuID;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "name", nullable = false, length = 200)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "price", nullable = false, length = 200)
    public Float getPrice() {
        return this.price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    @Column(name = "description", nullable = false, length = 200)
    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "menuID", nullable = false, length = 200)
    public Integer getMenuID() {
        return this.menuID;
    }

    public void setMenuID(Integer menuID) {
        this.menuID = menuID;
    }

}
