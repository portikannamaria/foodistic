package spring.demo.services;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.demo.dao.*;
import spring.demo.dto.*;
import spring.demo.entities.*;
import spring.demo.strategy.Strategy;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class OrderService implements Strategy {

    private static final String SAMPLE_CSV_FILE = "C:\\Users\\Annamari\\IdeaProjects\\PS_Assignment_3\\food\\orders.csv";
    @Autowired
    private UserDAO userDAO;

    @Autowired
    private FoodDAO foodDAO;

    @Autowired
    private OrderDAO orderDAO;

    @Autowired
    private MenuDAO menuDAO;

    @Autowired
    private FullOrderDAO fullOrderDAO;

    @Autowired
    private RestaurantDAO restaurantDAO;

    public List<MenuDTO> showMenus(int  restID) {
        List<Menu> menus = menuDAO.findById(restID,"restaurantID");
        List<MenuDTO> toReturn = new ArrayList<MenuDTO>();
        for (Menu menu : menus) {
            MenuDTO dto = new MenuDTO.Builder()
                    .id(menu.getId())
                    .name(menu.getName())
                    .restaurantID(menu.getRestaurantID())
                    .create();
            toReturn.add(dto);
        }
        return toReturn;
    }

    public List<FoodDTO> showFoods(int  menuID) {
        List<Food> foods = foodDAO.findById(menuID,"menuID");
        List<FoodDTO> toReturn = new ArrayList<FoodDTO>();
        for (Food food : foods) {
            FoodDTO dto = new FoodDTO.Builder()
                    .id(food.getId())
                    .name(food.getName())
                    .price(food.getPrice())
                    .description(food.getDescription())
                    .create();
            toReturn.add(dto);
        }
        return toReturn;
    }

    public int insertOrder(OrderDTO orderDTO){
        if(orderDTO.getBuc()!=0) {
            Order order = new Order();
            order.setFoodID(orderDTO.getFoodID());
            order.setAllOrdersID(0);
            order.setBuc(orderDTO.getBuc());
            return orderDAO.insert(order);
        }
        return 0;
    }

    @Override
    public List<BillDTO> showOrders(){
        List<Order> orders=orderDAO.findById(0,"allOrdersID");
        List<BillDTO> toReturn = new ArrayList<BillDTO>();
        for (Order o : orders) {
            Food f=foodDAO.findById(o.getFoodID(),"id").get(0);
            BillDTO dto = new BillDTO.Builder()
                    .food(f.getName())
                    .price(f.getPrice())
                    .buc(o.getBuc())
                    .create();
            toReturn.add(dto);
        }
        return toReturn;
    }

    public int placeOrders(int userId){
        float sum=0;
        List<Order> orders=orderDAO.findById(0,"allOrdersID");
        if(orders.isEmpty())
            return 0;
        for (Order o:orders) {
            sum+=o.getBuc()*foodDAO.findById(o.getFoodID(),"id").get(0).getPrice();
        }
        FullOrder fullOrder=new FullOrder();
        fullOrder.setUserID(userId);
        fullOrder.setDate(new Date());
        fullOrder.setSum(sum);
        fullOrder.setStatus("");
        int idFullOrder=fullOrderDAO.insert(fullOrder);
        for (Order o: orders) {
            o.setAllOrdersID(idFullOrder);
            orderDAO.update(o);
        }
        return 0;
    }

    public List<RestaurantDTO> findAllRestaurants(){
        List<Restaurant> restaurants = restaurantDAO.findALL();
        List<RestaurantDTO> toReturn = new ArrayList<RestaurantDTO>();
        for (Restaurant restaurant : restaurants) {
            RestaurantDTO dto = new RestaurantDTO.Builder()
                    .id(restaurant.getId())
                    .name(restaurant.getName())
                    .address(restaurant.getAddress())
                    .telephone(restaurant.getTelephone())
                    .create();
            toReturn.add(dto);
        }
        return toReturn;
    }

    public void clearCart(){
        List<Order> orders=orderDAO.findById(0,"allOrdersID");
        for(Order o:orders){
            orderDAO.delete(o.getId());
        }
    }

    public List<FullOrderDTO> getAllOrders(int id){
        List<FullOrder> allorders=fullOrderDAO.findById(id,"userID");
        List<FullOrderDTO> toReturn=new ArrayList<>();
        for(FullOrder fo:allorders){
            int foodId=orderDAO.findById(fo.getId(),"allOrdersID").get(0).getFoodID();
            int menuID=foodDAO.findById(foodId,"id").get(0).getMenuID();
            int restaurantID=menuDAO.findById(menuID,"id").get(0).getRestaurantID();
            String restaurant=restaurantDAO.findById(restaurantID,"id").get(0).getName();
            FullOrderDTO dto=new FullOrderDTO.Builder()
                    .id(fo.getId())
                    .userID(fo.getUserID())
                    .sum(fo.getSum())
                    .date(fo.getDate())
                    .status(fo.getStatus())
                    .restaurant(restaurant)
                    .create();
            toReturn.add(dto);
        }
        return toReturn;
    }

    public List<BillDTO> getOrdersForAllOrders(int id){
        List<Order> orders=orderDAO.findById(id,"allOrdersID");
        List<BillDTO> toReturn = new ArrayList<BillDTO>();
        for (Order o : orders) {
            Food f=foodDAO.findById(o.getFoodID(),"id").get(0);
            BillDTO dto = new BillDTO.Builder()
                    .food(f.getName())
                    .price(f.getPrice())
                    .buc(o.getBuc())
                    .create();
            toReturn.add(dto);
        }
        return toReturn;
    }

    public void exportOrders(){
        List<Order> orders=orderDAO.findALL();
        try (
                BufferedWriter writer = Files.newBufferedWriter(Paths.get(SAMPLE_CSV_FILE));

                CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT
                        .withHeader("ID","Customer","Food", "Full Order ID", "Buc","Price","Date"));
        ) {

            for(Order o:orders) {
                FullOrder fullOrder=fullOrderDAO.findById(o.getAllOrdersID(),"id").get(0);
                String name=userDAO.findById(fullOrder.getUserID(),"id").get(0).getName();
                Food food=foodDAO.findById(o.getFoodID(),"id").get(0);
                csvPrinter.printRecord(o.getId(),name, food.getName(), o.getAllOrdersID(), o.getBuc(),food.getPrice(),fullOrder.getDate());
            }

            csvPrinter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
