package spring.demo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.demo.dao.*;
import spring.demo.dto.FullOrderDTO;
import spring.demo.entities.*;

import java.util.ArrayList;
import java.util.List;

@Service
public class AdminService {


    @Autowired
    private AdminDAO adminDAO;

    @Autowired
    private FoodDAO foodDAO;

    @Autowired
    private OrderDAO orderDAO;

    @Autowired
    private MenuDAO menuDAO;

    @Autowired
    private FullOrderDAO fullOrderDAO;


    public List<FullOrderDTO> findOrdersByRestaurants(int id){
        List<FullOrderDTO> fullOrders=new ArrayList<>();
        Admin admin=adminDAO.findById(id,"id").get(0);
        int restId=admin.getRestaurantID();
        List<Menu> menus=menuDAO.findById(restId,"restaurantID");
        for(Menu m:menus)
        {
            List<Food> foods=foodDAO.findById(m.getId(),"menuID");
            for(Food f:foods){
                List<Order> orders=orderDAO.findById(f.getId(),"foodID");
                for(Order o:orders) {
                    List<FullOrder>fullOrderList=fullOrderDAO.findById(o.getAllOrdersID(),"id");
                    if(!fullOrderList.isEmpty()) {
                        FullOrder fo = fullOrderList.get(0);
                        System.out.println(fo.getId()+ " "+fo.getSum()+"\n" );
                        FullOrderDTO fodto = new FullOrderDTO.Builder()
                                .id(fo.getId())
                                .userID(fo.getUserID())
                                .sum(fo.getSum())
                                .date(fo.getDate())
                                .status(fo.getStatus())
                                .create();
                        boolean contains=false;
                        for(FullOrderDTO fi:fullOrders)
                            if(fi.getId()==fodto.getId())
                                contains=true;

                        if (!contains)
                            fullOrders.add(fodto);
                    }
                }
            }
        }
        return fullOrders;
    }

    public void changeOrderStatus(FullOrderDTO fullOrderDTO){
        FullOrder fullOrder=new FullOrder();
        fullOrder.setId(fullOrderDTO.getId());
        fullOrder.setUserID(fullOrderDTO.getUserID());
        fullOrder.setSum(fullOrderDTO.getSum());
        fullOrder.setDate(fullOrderDAO.findById(fullOrderDTO.getId(),"id").get(0).getDate());
        fullOrder.setStatus(fullOrderDTO.getStatus());
        fullOrderDAO.update(fullOrder);
    }

}
