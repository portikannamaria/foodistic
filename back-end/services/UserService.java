package spring.demo.services;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import spring.demo.dao.*;
import spring.demo.dto.*;
import spring.demo.entities.*;
import spring.demo.errorhandler.EntityValidationException;
import spring.demo.errorhandler.ResourceNotFoundException;

@Service
public class UserService {
	private static final String SPLIT_CH = " ";
	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
			Pattern.CASE_INSENSITIVE);


	@Autowired
	private UserDAO userDAO;

	@Autowired
	private AdminDAO adminDAO;

	public UserDTO findUserById(int userId) {
		User usr = userDAO.findById(userId,"id").get(0);
		if (usr == null) {
			throw new ResourceNotFoundException(User.class.getSimpleName());
		}
		String[] names = extractNames(usr.getName());

		UserDTO dto = new UserDTO.Builder()
				.id(usr.getId())
				.firstname(names[0])
				.surname(names[1])
				.city(usr.getCity())
				.address(usr.getAddress())
				.password(usr.getPassword())
				.email(usr.getEmail())
				.telephone(usr.getTelephone())
				.country(usr.getCountry())
				.postcode(usr.getPostcode())
				.create();
		return dto;
	}

	public int create(UserDTO userDTO) {
		List<String> validationErrors = validateUser(userDTO);
		if (!validationErrors.isEmpty()) {
			throw new EntityValidationException(User.class.getSimpleName(),validationErrors);
		}

		User user = new User();
		user.setName(userDTO.getFirstname().trim() + SPLIT_CH + userDTO.getSurname().trim());
		user.setEmail(userDTO.getEmail());
		user.setPassword(userDTO.getPassword());
		user.setAddress(userDTO.getAddress());
		user.setPostcode(userDTO.getPostcode());
		user.setCity(userDTO.getCity());
		user.setCountry(userDTO.getCountry());
		user.setTelephone(userDTO.getTelephone());
		return userDAO.insert(user);
	}

	public int logIn(String email, String passw){
		Admin admin=adminDAO.findByString(email,"name");
		if(admin!=null) {
			if (admin.getPassword().equals(passw))
				return admin.getId();
			return -1;
		}
		else {
			User usr = userDAO.findByString(email, "email");
			String pass = usr.getPassword();
			if (pass.equals(passw)) {
				return usr.getId();
			}
			return -1;
		}
	}

	public int updateUser(UserDTO userDTO){
		User user = new User();
		user.setId(userDTO.getId());
		user.setName(userDTO.getFirstname().trim() + SPLIT_CH + userDTO.getSurname().trim());
		user.setEmail(userDTO.getEmail());
		user.setPassword(userDTO.getPassword());
		user.setAddress(userDTO.getAddress());
		user.setPostcode(userDTO.getPostcode());
		user.setCity(userDTO.getCity());
		user.setCountry(userDTO.getCountry());
		user.setTelephone(userDTO.getTelephone());
		return userDAO.update(user);
	}
	private List<String> validateUser(UserDTO usr) {
		List<String> validationErrors = new ArrayList<String>();

		if (usr.getFirstname() == null || "".equals(usr.getFirstname())) {
			validationErrors.add("First Name field should not be empty");
		}

		if (usr.getSurname() == null || "".equals(usr.getSurname())) {
			validationErrors.add("Surname field should not be empty");
		}

		if (usr.getEmail() == null || !validateEmail(usr.getEmail())) {
			validationErrors.add("Email does not have the correct format.");
		}

		return validationErrors;
	}

	public static boolean validateEmail(String email) {
		Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(email);
		return matcher.find();
	}
	private String[] extractNames(String fullname){
		String[] names = new String[2];
		int surnameIndex = fullname.lastIndexOf(SPLIT_CH);
		names[0] = fullname;
		names[1] = "";
		if (surnameIndex != -1) {
			names[0] = fullname.substring(0, surnameIndex).trim();
			names[1] = fullname.substring(surnameIndex).trim();
		}
		return names;
	}

}
