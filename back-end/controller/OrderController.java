package spring.demo.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import spring.demo.dto.*;
import spring.demo.services.OrderService;
import spring.demo.strategy.Cache;
import spring.demo.strategy.Context;

import java.util.List;


@RestController
@RequestMapping("/order")
public class OrderController {
    @Autowired
    private OrderService orderService;
    private long lastTime=-30000;

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/restaurants", method = RequestMethod.GET)
    public @ResponseBody
    List<RestaurantDTO> showAllRestaurants() {
        return orderService.findAllRestaurants();
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/cart", method = RequestMethod.POST)
    public @ResponseBody
    int insertOrder(@RequestBody OrderDTO orderDTO) {
        Cache cache=Cache.getInstance();
        cache.orderDTOS.clear();
        lastTime=0;
        return orderService.insertOrder(orderDTO);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/clearcart", method = RequestMethod.POST)
    public
    void clearCart() {
        Cache cache=Cache.getInstance();
        cache.orderDTOS.clear();
        lastTime=0;
        orderService.clearCart();
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/orders", method = RequestMethod.GET)
    public @ResponseBody
    List<BillDTO> ordersInCart() {
        long currentTime=System.currentTimeMillis();
        if((currentTime-lastTime)/1000>30)
        {
            Cache cache=Cache.getInstance();
            Context c=new Context(orderService);
            lastTime=currentTime;
            cache.orderDTOS.clear();
            System.out.println("Service\n");
            List<BillDTO> restaurants=c.executeStrategy();
            cache.orderDTOS=restaurants;
            return restaurants;
        }
        else
        {
            Context c=new Context(Cache.getInstance());
            lastTime=currentTime;
            System.out.println("Cache\n");
            return c.executeStrategy();
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/saveorder", method = RequestMethod.GET)
    public @ResponseBody
    int saveOrder(@RequestParam(name = "id") int id) {
        Cache cache=Cache.getInstance();
        cache.orderDTOS.clear();
        lastTime=0;
        int ret=orderService.placeOrders(id);
        orderService.exportOrders();
        return ret;
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/menus", method = RequestMethod.GET)
    public @ResponseBody List<MenuDTO> getMenus(@RequestParam(name = "id") int id) {
        return orderService.showMenus(id);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/foods", method = RequestMethod.GET)
    public @ResponseBody List<FoodDTO> getFoods(@RequestParam(name = "id") int id) {
        return orderService.showFoods(id);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/allorders", method = RequestMethod.GET)
    public @ResponseBody List<FullOrderDTO> geAllOrders(@RequestParam(name = "id") int id) {
        return orderService.getAllOrders(id);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/listorder", method = RequestMethod.GET)
    public @ResponseBody List<BillDTO> getSuborder(@RequestParam(name = "id") int id) {
        return orderService.getOrdersForAllOrders(id);
    }
}
