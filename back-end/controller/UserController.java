package spring.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import spring.demo.dto.UserDTO;
import spring.demo.services.UserService;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/details", method = RequestMethod.GET)
	public @ResponseBody UserDTO getUserById(@RequestParam(name = "userId") int userId) {
		return userService.findUserById(userId);
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(value = "/change", method = RequestMethod.POST)
	public @ResponseBody int updateUser(@RequestBody UserDTO userDTO) {
		return userService.updateUser(userDTO);
	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public @ResponseBody int insertUser(@RequestBody UserDTO userDTO) {
		return userService.create(userDTO);
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public @ResponseBody int logIn(@RequestParam(name = "email") String email,@RequestParam(name = "password")  String password) {
		return userService.logIn(email,password);
	}

}

