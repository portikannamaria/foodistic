package spring.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import spring.demo.dto.FullOrderDTO;
import spring.demo.services.AdminService;
import spring.demo.services.UserService;

import java.util.List;

@RestController
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    private AdminService adminService;

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/orders", method = RequestMethod.GET)
    public @ResponseBody
    List<FullOrderDTO> getAllOrders(@RequestParam(name = "id") int id) {
        return adminService.findOrdersByRestaurants(id);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/statuschange", method = RequestMethod.POST)
    public void updateUser(@RequestBody FullOrderDTO fullOrderDTO) {
        adminService.changeOrderStatus(fullOrderDTO);
    }


}
