package spring.demo.dao;

import org.springframework.stereotype.Component;
import spring.demo.entities.Food;

@Component("foodDAO")
public class FoodDAO extends AbstractDAO<Food>{

}
