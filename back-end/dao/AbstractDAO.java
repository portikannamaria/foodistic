package spring.demo.dao;

import org.hibernate.*;

import org.springframework.beans.factory.annotation.Autowired;


import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
public abstract class AbstractDAO<T> {
    protected static final Logger LOGGER = Logger.getLogger(spring.demo.dao.AbstractDAO.class.getName());

    @Autowired
    public SessionFactory sessionFactory;

    private final Class<T> type;

    public AbstractDAO() {
        this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }
    public ArrayList<T> findById(int id, String field) {
        Transaction tx = null;
        Session session = sessionFactory.openSession();
        try{
            tx = session.beginTransaction();
            String hql = ("FROM "+type.getSimpleName()+ " WHERE "+field+" = :value" );
            Query query = session.createQuery(hql);
            query.setParameter("value",id);
            ArrayList<T> l=(ArrayList<T>) query.list();
            tx.commit();
            return l;
        }catch(HibernateException e){
            if(null != tx){
                tx.rollback();
                LOGGER.log(Level.WARNING, type.getName() + "findByID " + e.getMessage());
            }
        }finally{
            session.close();
        }
        return null;
    }

    public T findByString(String id, String field) {
        Transaction tx = null;
        Session session = sessionFactory.openSession();
        try{
            tx = session.beginTransaction();
            String hql = ("FROM "+type.getSimpleName()+ " WHERE "+field+" = :value" );
            Query query = session.createQuery(hql);
            query.setParameter("value",id);
            ArrayList<T> l=(ArrayList<T>) query.list();
            tx.commit();
            if(l.size()==0)
                return null;
            return l.get(0);
        }catch(HibernateException e){
            if(null != tx){
                tx.rollback();
                LOGGER.log(Level.WARNING, type.getName() + "findByID " + e.getMessage());
            }
        }finally{
            session.close();
        }
        return null;
    }


    public ArrayList<T> findALL() {
        Transaction tx = null;
        Session session = sessionFactory.openSession();
        try{
            tx = session.beginTransaction();
            String hql ="FROM "+type.getSimpleName();
            Query query = session.createQuery(hql);
            ArrayList<T> l= (ArrayList<T>) query.list();
            tx.commit();
            return l;
        }catch(HibernateException e){
            if(null != tx){
                tx.rollback();
                LOGGER.log(Level.WARNING, type.getName() + "findALL " + e.getMessage());
            }
        }finally{
            session.close();
        }
        return null;
    }

    public int insert(Object o) {
        Transaction tx = null;
        Session session = sessionFactory.openSession();
        try{
            tx = session.beginTransaction();
            int id= (int) session.save(o);
            tx.commit();
            return id;
        }catch(HibernateException e){
            if(null != tx){
                tx.rollback();
                LOGGER.log(Level.WARNING, type.getName() + "insert " + e.getMessage());
            }
        }finally{
            session.close();
        }
        return -1;
    }

    public int delete(int id){
        Transaction tx = null;
        Session session = sessionFactory.openSession();
        try{
            tx = session.beginTransaction();
            Object o = session.get(type, id);
            session.delete(o);
            tx.commit();
            return 1;
        }catch(HibernateException e){
            if(null != tx){
                tx.rollback();
                LOGGER.log(Level.WARNING, type.getName() + " delete " + e.getMessage());
            }
        }finally{
            session.close();
        }
        return 0;
    }


}
