package spring.demo.dao;


import org.springframework.stereotype.Component;
import spring.demo.entities.Restaurant;

@Component("restaurantDAO")
public class RestaurantDAO extends AbstractDAO<Restaurant>{
}
