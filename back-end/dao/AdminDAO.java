package spring.demo.dao;

import org.springframework.stereotype.Component;
import spring.demo.entities.Admin;

@Component("adminDAO")
public class AdminDAO extends AbstractDAO<Admin>{
}
