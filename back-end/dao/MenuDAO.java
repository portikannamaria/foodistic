package spring.demo.dao;

import org.springframework.stereotype.Component;
import spring.demo.entities.Menu;

@Component("menuDAO")
public class MenuDAO extends AbstractDAO<Menu>{

}
