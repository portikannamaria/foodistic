package spring.demo.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import spring.demo.entities.FullOrder;
import spring.demo.entities.User;

import java.util.logging.Level;

@Component ("fullOrderDAO")
public class FullOrderDAO extends AbstractDAO<FullOrder>{
    @Autowired
    public SessionFactory sessionFactory;

    public int update(FullOrder u){
        Transaction tx = null;
        Session session = sessionFactory.openSession();
        try{
            tx = session.beginTransaction();
            session.update(u);
            tx.commit();
            return 0;
        }catch(Exception e){
            if(null != tx){
                tx.rollback();
                LOGGER.log(Level.WARNING,  e.getMessage());
            }
        }finally{
            session.close();
        }
        return -1;
    }
}
