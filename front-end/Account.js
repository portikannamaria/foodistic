
import React, { Component } from 'react';
import * as Colors from 'material-ui/styles/colors';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import RaisedButton from 'material-ui/RaisedButton';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Grid from '@material-ui/core/Grid';
import TextField from 'material-ui/TextField';
import Home from "./Restaurants";
import axios from 'axios'

const muiTheme = getMuiTheme({
    palette: {
      textColor: Colors.white,
      primary1Color: Colors.white,
      primary2Color: Colors.cyan500,
      accent1Color: Colors.redA200,
      pickerHeaderColor: Colors.white,
      alternateTextColor: Colors.cyan500,
    },
    
  });

class Account extends Component {

    constructor(props) {
        super(props)
        this.state = {
            user: [],
            message: null,
            auth: true,
            anchorEl: null,
            id:-1,
            firstname:'',
            lastname:'',
            email:'',
            password:'',
            address:'',
            postcode:'',
            city:'',
            country:'',
            telephone:''
        }
        this.getAccount = this.getAccount.bind(this)
    }
    
    handleMessage(message) {

        let { messages } = this.state

        this.setState({ messages: [...messages, message.content] })

    }
      handleClick(event){
        var self = this;
        var userLoad={
            "id":this.state.id,
            "firstname": this.state.firstname,
            "surname":this.state.lastname,
            "email":this.state.email,
            "password":this.state.password,
            "address":this.state.address,
            "postcode":this.state.postcode,
            "city":this.state.city,
            "country":this.state.country,
            "telephone":this.state.telephone
            }
         axios.post('http://localhost:8080/spring_demo_war/user/change', userLoad)
        .then(
                response => {
                    console.log(response);
                }
            )
      }

      handleBack(){
        var self = this;
        var home=[];
        home.push(<Home appContext={self.props.appContext}/>)
        self.props.appContext.setState({account:[],home:home})
      }


    componentDidMount() {
        this.getAccount();
    }

    getAccount(){
        var self = this;
         axios.get('http://localhost:8080/spring_demo_war/user/details', {
            params: {
             userId:self.props.appContext.state.accountId
             }})
        .then(
                response => {
                    console.log(response);
                    this.setState({ user: response.data })
                    this.setState({
                        id:this.state.user.id,
                        firstname:this.state.user.firstname,
                        lastname:this.state.user.surname,
                        email:this.state.user.email,
                        password:this.state.user.password,
                        address:this.state.user.address,
                        postcode:this.state.user.postcode,
                        city:this.state.user.city,
                        country:this.state.user.country,
                        telephone:this.state.user.telephone})
                }
            )
    }
    render() {
        window.history.pushState("object or string", "Title", "/account");
        const { anchorEl } = this.state;
       /* const notification = self.props.appContext.state.messages.map((message, index) => (

            <SnackbarContent key={index} className={classes.snackbar} message={message} />

        ))*/
        return (
            <div>
                <MuiThemeProvider muiTheme={muiTheme}>
                    <div>
                        <AppBar
                            style={{ backgroundColor: "#00bcd4"}}
                            position="static"
                        >
                            <Toolbar>
                                <Typography variant="h6" color="inherit">
                                Foodistic
                                </Typography>
                            </Toolbar>
                        </AppBar>
                        <Grid
                            container
                            spacing={0}
                            direction="column"
                            alignItems="center"
                            justify="center"
                            style={{ minHeight: '40vh' }}
                            >
                            <h2>Edit account</h2>
                            <div>
                                <TextField
                                    label="Firstname"
                                    defaultValue={this.state.user.firstname}
                                    margin="normal"
                                    onChange = {(event,newValue) => this.setState({firstname:newValue})}
                                />
                            <br/>
                                <TextField
                                        label="Surname"
                                        defaultValue={this.state.user.surname}
                                        margin="normal"
                                        onChange = {(event,newValue) => this.setState({lastname:newValue})}
                                    />
                                <br/>
                                <TextField
                                         label="Email"
                                         defaultValue={this.state.user.email}
                                         margin="normal"
                                         onChange = {(event,newValue) => this.setState({email:newValue})}
                                    />
                                <br/>
                                <TextField
                                        type="password"
                                        label="Password"
                                        defaultValue={this.state.user.password}
                                        margin="normal"
                                        onChange = {(event,newValue) => this.setState({password:newValue})}
                                    />
                                <br/>
                                <TextField
                                         label="Address"
                                         defaultValue={this.state.user.address}
                                         margin="normal"
                                         onChange = {(event,newValue) => this.setState({address:newValue})}
                                    />
                                <br/>
                                <TextField
                                         label="Postcode"
                                         defaultValue={this.state.user.postcode}
                                         margin="normal"
                                         onChange = {(event,newValue) => this.setState({postcode:newValue})}
                                    />
                                <br/>
                                <TextField
                                         label="City"
                                         defaultValue={this.state.user.city}
                                         margin="normal"
                                         onChange = {(event,newValue) => this.setState({city:newValue})}
                                    />
                                <br/>
                                <TextField
                                         label="Country"
                                         defaultValue={this.state.user.country}
                                         margin="normal"
                                         onChange = {(event,newValue) => this.setState({country:newValue})}
                                    />
                                <br/>
                                <TextField
                                         label="Telephone"
                                         defaultValue={this.state.user.telephone}
                                         margin="normal"
                                         onChange = {(event,newValue) => this.setState({telephone:newValue})}
                                    />
                                <br/>
                                <RaisedButton label="Save edit" style={style} onClick={(event) => this.handleClick(event)}/>
                                <RaisedButton label="Back" style={style} onClick={(event) => this.handleBack()}/>
                            </div>
                        </Grid>
                                    
                    </div>
                </MuiThemeProvider>
            </div> 
        )
    }
}

const style = {
    margin: 15,
   };

export default Account

