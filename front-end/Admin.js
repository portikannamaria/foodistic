
import React, { Component } from 'react';
import * as Colors from 'material-ui/styles/colors';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import RaisedButton from 'material-ui/RaisedButton';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';
import TextField from 'material-ui/TextField';
import Login from "./App";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import axios from 'axios'

const muiTheme = getMuiTheme({
    palette: {
      textColor: Colors.black,
      primary1Color: Colors.black,
      primary2Color: Colors.cyan500,
      accent1Color: Colors.redA200,
      pickerHeaderColor: Colors.black,
      alternateTextColor: Colors.cyan500,
    },
  });
class Admin extends Component {

    constructor(props) {
        super(props)
        this.state = {
            orders: [],
            status:[],
        }
        this.getOrders = this.getOrders.bind(this)
    }
    
    
      handleBack = () => {
        var self = this;
        var login=[];
        login.push(<Login appContext={self.props.appContext}/>)
        self.props.appContext.setState({loginPage:login,admin:[]})
      };

    
      handleSave=(id,userID,sum)=>{
        var self = this;
        var orderLoad={
            "id":id,
            "userID":userID,
            "sum":sum,
            "date":null,
            "status":this.state.status
            }
         axios.post('http://localhost:8080/spring_demo_war/admin/statuschange', orderLoad)
        .then(
                response => {
                    console.log(response);
                }
            )
      }
      
    componentDidMount() {
        this.getOrders();
    }

    getOrders(){
        var self = this;
         axios.get('http://localhost:8080/spring_demo_war/admin/orders', {
            params: {
             id:self.props.appContext.state.adminId
             }})
        .then(
                response => {
                    console.log(response);
                    this.setState({ orders: response.data })
                }
            )
    }
    render() {
        window.history.pushState("object or string", "Title", "/foods");
        return (
            <div>
                <MuiThemeProvider muiTheme={muiTheme}>
                    <div>
                        <AppBar
                            style={{ backgroundColor: "#00bcd4"}}
                            position="static"
                        >
                            <Toolbar>
                                <Typography variant="h6" color="inherit">
                                Foodistic
                                </Typography> 
                            </Toolbar>     
                        </AppBar>
                        <div>
                        <Grid
                            container
                            spacing={0}
                            direction="column"
                            alignItems="center"
                            justify="center"
                            style={{ minHeight: '40vh' }}
                            >
                            <h2>Orders</h2>
                                <Paper>
                                    <Table>
                                         <TableHead>
                                            <TableRow>
                                                  <TableCell>OrderID</TableCell>
                                                  <TableCell align="right">UserID</TableCell>
                                                  <TableCell align="right">Sum</TableCell>
                                                  <TableCell align="right">Date</TableCell>
                                                  <TableCell >Status</TableCell>
                                            </TableRow>
                                          </TableHead>
                                          <TableBody>
                                             { this.state.orders.map(
                                                o=> (
                                                  <TableRow key={o.id}>
                                                    <TableCell component="th" scope="row">
                                                      {o.id}
                                                    </TableCell>
                                                    <TableCell align="right">{o.userID}</TableCell>
                                                    <TableCell align="right">{o.sum}</TableCell>
                                                    <TableCell align="right">{o.date}</TableCell>
                                                    <TableCell align="right">
                                                        <TextField
                                                            name="status"
                                                            label="Firstname"
                                                            defaultValue={o.status}
                                                            margin="normal"
                                                            onChange = {(event,newValue) => this.setState({status:newValue})}/>
                                                    </TableCell>
                                                    <RaisedButton label="Save" style={style} onClick={()=>this.handleSave(o.id,o.userID,o.sum)}/>
                                                  </TableRow>
                                                ))}
                                            </TableBody>
                                    </Table>
                                </Paper>
                            
                        </Grid>
                        </div>
                        <Grid
                            container
                            spacing={0}
                            direction="column"
                            alignItems="center"
                            justify="center"
                            style={{ minHeight: '40vh' }}
                            >
                            <RaisedButton label="Logout" style={style} onClick={(event) => this.handleBack()}/>
                        </Grid>
                    </div>
                </MuiThemeProvider>
            </div> 
        )
    }
}

const style = {
    margin: 15,
   };

export default Admin

