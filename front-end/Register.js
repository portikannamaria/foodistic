import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import * as Colors from 'material-ui/styles/colors';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import axios from 'axios';
import Login from "./Login";

const muiTheme = getMuiTheme({
    palette: {
      textColor: Colors.white,
      primary1Color: Colors.white,
      primary2Color: Colors.cyan500,
      accent1Color: Colors.redA200,
      pickerHeaderColor: Colors.white,
      alternateTextColor: Colors.cyan500,
    },
    
  });

class Register extends Component {

  constructor(props){
    super(props);
    this.state={
      first_name:'',
      last_name:'',
      email:'',
      password:'',
      address:'',
      postcode:'',
      city:'',
      country:'',
      telephone:''
    }
  }

  handleClick(event){
    var apiBaseUrl = 'http://localhost:8080/spring_demo_war/user/insert';
    console.log("values",this.state.first_name,this.state.last_name,this.state.email,this.state.password,this.state.address,this.state.postcode,this.state.city,this.state.country,this.state.telephone);
    //To be done:check for empty values before hitting submit
    var self = this;

    var payload={
    "firstname": this.state.first_name,
    "surname":this.state.last_name,
    "email":this.state.email,
    "password":this.state.password,
    "address":this.state.address,
    "postcode":this.state.postcode,
    "city":this.state.city,
    "country":this.state.country,
    "telephone":this.state.telephone
    }

    axios.post(apiBaseUrl, payload)
   .then(function (response) {
     console.log(response);
     if(response.data.code == 200){
       console.log("registration successfull");
       var loginscreen=[];
       loginscreen.push(<Login parentContext={this}/>);
       var loginmessage = "Not Registered yet.Go to registration";
       self.props.parentContext.setState({loginscreen:loginscreen,
       loginmessage:loginmessage,
       buttonLabel:"Register",
       isLogin:true
        });
     }
   })
   .catch(function (error) {
    if (error.response) {
      // Request made and server responded
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    } else if (error.request) {
      // The request was made but no response was received
      console.log(error.request);
    } else {
      // Something happened in setting up the request that triggered an Error
      console.log('Error', error.message);
    }

  });
  }

  render() {
    window.history.pushState("object or string", "Title", "/register");
    return (
      <div>
       <MuiThemeProvider muiTheme={muiTheme}>
              <div>
            <AppBar
                style={{ backgroundColor: "#00bcd4"}}
                position="static"
            >
                <Toolbar>
                    <Typography variant="h6" color="inherit">
                    Foodistic
                    </Typography>
                </Toolbar>
            </AppBar>
            <h2>Register</h2>
           <TextField
             hintText="Enter your First Name"
             floatingLabelText="First Name"
             onChange = {(event,newValue) => this.setState({first_name:newValue})}
             />
           <br/>
           <TextField
             hintText="Enter your Last Name"
             floatingLabelText="Last Name"
             onChange = {(event,newValue) => this.setState({last_name:newValue})}
             />
           <br/>
           <TextField
             hintText="Enter your Email"
             type="email"
             floatingLabelText="Email"
             onChange = {(event,newValue) => this.setState({email:newValue})}
             />
           <br/>
           <TextField
             type = "password"
             hintText="Enter your Password"
             floatingLabelText="Password"
             onChange = {(event,newValue) => this.setState({password:newValue})}
             />
           <br/>
           <TextField
             hintText="Enter your Address"
             floatingLabelText="Address"
             onChange = {(event,newValue) => this.setState({address:newValue})}
             />
           <br/>
           <TextField
             hintText="Enter your Telephone"
             floatingLabelText="Telephone"
             onChange = {(event,newValue) => this.setState({telephone:newValue})}
             />
           <br/>
           <RaisedButton label="Sign up" style={style} onClick={(event) => this.handleClick(event)}/>
          </div>
         </MuiThemeProvider>
      </div>
    );
  }
}
const style = {
  margin: 15,
};
export default Register;