
import React, { Component } from 'react';
import * as Colors from 'material-ui/styles/colors';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import RaisedButton from 'material-ui/RaisedButton';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import TextField from 'material-ui/TextField';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import SvgIcon from '@material-ui/core/SvgIcon';
import Login from "./App";
import Menus from "./Menus";
import Order from "./Cart";
import axios from 'axios'

const muiTheme = getMuiTheme({
    palette: {
      textColor: Colors.black,
      primary1Color: Colors.black,
      primary2Color: Colors.cyan500,
      accent1Color: Colors.redA200,
      pickerHeaderColor: Colors.black,
      alternateTextColor: Colors.cyan500,
    },
  });
class Foods extends Component {

    constructor(props) {
        super(props)
        this.state = {
            food: [],
            message: null,
            auth: true,
            anchorEl: null,
            buc:0,
            allOrdID:0,
        }
        this.getFoods = this.getFoods.bind(this)
    }
    
      handleMenu = event => {
        this.setState({ anchorEl: event.currentTarget });
      };
    
      handleLogout = () => {
        var self = this;
        var login=[];
        login.push(<Login appContext={self.props.appContext}/>)
        self.props.appContext.setState({loginPage:login,home:[],menu:[]})
      };

      handleCart(){
        var self = this;
        var order=[];
        order.push(<Order appContext={self.props.appContext}/>)
        self.props.appContext.setState({order:order,food:[]})
      } 

      handleSave=(food,all)=>{
        var payload={
            "foodID": food,
            "allOrdersID":all,
            "buc":this.state.buc
            }
        axios.post('http://localhost:8080/spring_demo_war/order/cart',payload).then(
            response => {
                console.log(response);
            }
        )
      }

      handleBack(){
        var self = this;
        var menu=[];
        menu.push(<Menus appContext={self.props.appContext}/>)
        self.props.appContext.setState({menu:menu,food:[]})
      }


    componentDidMount() {
        this.getFoods();
    }

    getFoods(){
        var self = this;
         axios.get('http://localhost:8080/spring_demo_war/order/foods', {
            params: {
             id:self.props.appContext.state.menuId
             }})
        .then(
                response => {
                    console.log(response);
                    this.setState({ food: response.data })
                }
            )
    }
    render() {
        window.history.pushState("object or string", "Title", "/foods");
        const { anchorEl } = this.state;
        const open = Boolean(anchorEl);
        return (
            <div>
                <MuiThemeProvider muiTheme={muiTheme}>
                    <div>
                        <AppBar
                            style={{ backgroundColor: "#00bcd4"}}
                            position="static"
                        >
                            <Toolbar>
                                <Typography variant="h6" color="inherit">
                                Foodistic
                                </Typography> 
                                <RaisedButton label="Cart" style={style} onClick={(event) => this.handleCart()}/>
                            </Toolbar>     
                        </AppBar>
                        <div>
                        <Grid
                            container
                            spacing={0}
                            direction="column"
                            alignItems="center"
                            justify="center"
                            style={{ minHeight: '40vh' }}
                            >
                            <h2>Foods</h2>
                                <List component="nav">
                                    <div>
                                    {
                                        this.state.food.map(
                                            f=>
                                                <div style={{ background: "#FFFFFF" }} key={f.id}>
                                                    <ListItem>
                                                    <ListItemText primary={f.name} secondary={f.price+" Lei"}/>
                                                    </ListItem>
                                                    <TextField
                                                        id="standard-number"
                                                        onChange = {(event,newValue) => this.setState({buc:newValue})}
                                                        type="number"
                                                        margin="normal"
                                                        defaultValue="0"
                                                        style = {{width: 30}} 
                                                        />
                                                    <IconButton color="primary" onClick={()=>this.handleSave(f.id,0)} >
                                                        <AddShoppingCartIcon />
                                                    </IconButton>
                                                    <Divider />
                                                </div>
                                        )
                                    }
                                </div>
                                </List>
                            
                        </Grid>
                        </div>
                        <Grid
                            container
                            spacing={0}
                            direction="column"
                            alignItems="center"
                            justify="center"
                            style={{ minHeight: '40vh' }}
                            >
                            <RaisedButton label="Back" style={style} onClick={(event) => this.handleBack()}/>
                        </Grid>
                    </div>
                </MuiThemeProvider>
            </div> 
        )
    }
}

const style = {
    margin: 15,
   };

export default Foods

