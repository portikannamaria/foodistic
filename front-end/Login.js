import React, { Component } from "react";
import "./Login.css";
import axios from 'axios'
import Admin from "./Admin"
import Home from './Restaurants'
import Grid from '@material-ui/core/Grid';
import * as Colors from 'material-ui/styles/colors';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'

const muiTheme = getMuiTheme({
    palette: {
      textColor: Colors.white,
      primary1Color: Colors.white,
      primary2Color: Colors.cyan500,
      accent1Color: Colors.redA200,
      pickerHeaderColor: Colors.white,
      alternateTextColor: Colors.cyan500,
    },
  });

class Login extends Component {

constructor(props){
  super(props);
  this.state={
  email:'',
  password:''
  }
  this.handleClick = this.handleClick.bind(this);
 }


 handleClick(){
    var self = this;
    axios.get('http://localhost:8080/spring_demo_war/user/login', {
        params: {
         email:this.state.email,
         password:this.state.password}})
    .then(function (response) {
    console.log(response);
    if(response.data != -1){
      console.log("Login successfull");
      if(response.data >=10000)
      {
        self.props.appContext.setState({adminId:response.data});
        var admin=[];
        admin.push(<Admin appContext={self.props.appContext}/>);
        self.props.appContext.setState({loginPage:[],admin:admin})
      }
      else{
        self.props.appContext.setState({accountId:response.data});
        var home=[];
        home.push(<Home appContext={self.props.appContext}/>);
        self.props.appContext.setState({loginPage:[],home:home})
      }
    }
    else if (response.status === 400 || response.status === 500) {
        console.log( response.json());
      }
    })
    .catch(function (error) {
    console.log(error);
    });
    }

render() {
  window.history.pushState("object or string", "Title", "/login");
    return (
      <div>
          <MuiThemeProvider muiTheme={muiTheme}>
              <div>
                <AppBar
                    style={{ backgroundColor: "#00bcd4"}}
                    position="static"
                >
                    <Toolbar>
                        <Typography variant="h6" color="inherit">
                        Foodistic
                        </Typography>
                    </Toolbar>
                </AppBar>
                <Grid
                  container
                  spacing={0}
                  direction="column"
                  alignItems="center"
                  justify="center"
                  style={{ minHeight: '40vh' }}
                >
               <h2>Login</h2>
                <TextField
                    hintText="Enter your Email"
                    floatingLabelText="Email address"
                    onChange = {(event,newValue) => this.setState({email:newValue})}
                />
                <br/>
                    <TextField
                    type="password"
                    hintText="Enter your Password"
                    floatingLabelText="Password"
                    onChange = {(event,newValue) => this.setState({password:newValue})}
                    />
                    <br/>
                    <RaisedButton label="Log in" style={style} onClick={(event) => this.handleClick(event)}/>
                  </Grid>
                </div>
            </MuiThemeProvider>
    </div>
    );
  }
}
const style = {
 margin: 15,

};

const styles = theme => ({
    multilineColor:{
        color:'white'
    }
});
export default Login;