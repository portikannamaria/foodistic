
import React, { Component } from 'react';
import * as Colors from 'material-ui/styles/colors';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import RaisedButton from 'material-ui/RaisedButton';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Login from "./App";
import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import axios from 'axios'
import AllOrders from './AllOrders';

const muiTheme = getMuiTheme({
    palette: {
      textColor: Colors.black,
      primary1Color: Colors.black,
      primary2Color: Colors.cyan500,
      accent1Color: Colors.redA200,
      pickerHeaderColor: Colors.black,
      alternateTextColor: Colors.cyan500,
    },
  });
class OrderList extends Component {

    constructor(props) {
        super(props)
        this.state = {
            orders: [],
        }
        this.getOrders = this.getOrders.bind(this)
    }
    

      handleBack(){
        var self = this;
        var allorders=[];
        allorders.push(<AllOrders appContext={self.props.appContext}/>)
        self.props.appContext.setState({orderlist:[],allorders:allorders})
      }


    componentDidMount() {
        this.getOrders();
    }

    getOrders=()=>{
         axios.get('http://localhost:8080/spring_demo_war/order/listorder',{params:{id:this.props.appContext.state.allordersid}})
        .then(
                response => {
                    console.log(response);
                    this.setState({ orders: response.data })
                }
            )
    }
    render() {
        window.history.pushState("object or string", "Title", "/myorders");
        return (
            <div>
                <MuiThemeProvider muiTheme={muiTheme}>
                    <div>
                        <AppBar
                            style={{ backgroundColor: "#00bcd4"}}
                            position="static"
                        >
                            <Toolbar>
                                <Typography variant="h6" color="inherit">
                                Foodistic
                                </Typography>
                            </Toolbar>
                        </AppBar>
                        <h2>My Orders</h2>
                                <Paper style={{display: 'inline-block'}}>
                                    <Table >
                                    <h2>My Orders</h2>
                                        <TableHead>
                                        <TableRow>
                                            <TableCell >Food</TableCell>
                                            <TableCell >Price</TableCell>
                                            <TableCell >Buc</TableCell>
                                         </TableRow>
                                        </TableHead>
                                        <TableBody>
                                        {this.state.orders.map(order => (
                                            //this.getOrders(order.id),
                                            //this.state.orders.map(o => (
                                                <TableRow key={order.food}>
                                                <TableCell>{order.food}</TableCell>
                                                <TableCell >{order.price}</TableCell>
                                                <TableCell >{order.buc}</TableCell>
                                                </TableRow>
                                           // )),
                                            /*<TableRow>
                                                <TableCell rowSpan={3} />
                                                <TableCell colSpan={2}>Total</TableCell>
                                                <TableCell align="right">{order.sum}</TableCell>
                                            </TableRow>,
                                            <TableRow>
                                                <TableCell>Date</TableCell>
                                                <TableCell align="right">{order.date}</TableCell>
                                            </TableRow>,
                                            <TableRow>
                                                <TableCell colSpan={2}>Status</TableCell>
                                                <TableCell align="right">{order.status}</TableCell>
                                            </TableRow>*/
                                        ))}
                                        </TableBody>
                                    
                                    </Table>
                                </Paper>
                                <Grid
                            container
                            spacing={0}
                            direction="column"
                            alignItems="center"
                            justify="center"
                            style={{ minHeight: '40vh' }}
                            >
                            <RaisedButton label="Back" style={style} onClick={(event) => this.handleBack()}/>
                        </Grid>
                    </div>
                </MuiThemeProvider>
            </div> 
        )
    }
}

const style = {
    margin: 15,
   };

export default OrderList

