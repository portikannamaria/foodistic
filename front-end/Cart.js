
import React, { Component } from 'react';
import * as Colors from 'material-ui/styles/colors';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import RaisedButton from 'material-ui/RaisedButton';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import Login from "./App";
import axios from 'axios'
import Restaurants from './Restaurants';

const muiTheme = getMuiTheme({
    palette: {
      textColor: Colors.black,
      primary1Color: Colors.black,
      primary2Color: Colors.cyan500,
      accent1Color: Colors.redA200,
      pickerHeaderColor: Colors.black,
      alternateTextColor: Colors.cyan500,
    },
  });
class Cart extends Component {

    constructor(props) {
        super(props)
        this.state = {
            orders: [],
            message: null,
            auth: true,
            anchorEl: null,
            ordersID:0,
            fooID:-1,
            fullOrderID:0,
            bucO: 0
        }
        this.getOrders = this.getOrders.bind(this)
    }
    
    
      handleLogout = () => {
        var self = this;
        var login=[];
        login.push(<Login appContext={self.props.appContext}/>)
        self.props.appContext.setState({loginPage:login,home:[],menu:[]})
      };

      handleSave(){
          var self=this;
          axios.get('http://localhost:8080/spring_demo_war/order/saveorder', {
            params: {
             id:self.props.appContext.state.accountId
             }}).then(
            response => {
                console.log(response);}
        )

      }

      handleBack(){
        var self = this;
        var restaurant=[];
        restaurant.push(<Restaurants appContext={self.props.appContext}/>)
        self.props.appContext.setState({order:[],home:restaurant})
      }


    componentDidMount() {
        this.getOrders();
    }

    getOrders(){
         axios.get('http://localhost:8080/spring_demo_war/order/orders')
        .then(
                response => {
                    console.log(response);
                    this.setState({ orders: response.data })
                }
            )
    }
    render() {
        window.history.pushState("object or string", "Title", "/cart");
        const { anchorEl } = this.state;
        const open = Boolean(anchorEl);
        return (
            <div>
                <MuiThemeProvider muiTheme={muiTheme}>
                    <div>
                        <AppBar
                            style={{ backgroundColor: "#00bcd4"}}
                            position="static"
                        >
                            <Toolbar>
                                <Typography variant="h6" color="inherit">
                                Foodistic
                                </Typography>
                            </Toolbar>
                        </AppBar>
                        <div>
                        <Grid
                            container
                            spacing={0}
                            direction="column"
                            alignItems="center"
                            justify="center"
                            style={{ minHeight: '40vh' }}
                            >
                            <h2>Orders</h2>
                                <List component="nav">
                                    <div>
                                    {
                                        this.state.orders.map(
                                            f=>
                                                <div style={{ background: "#FFFFFF" }}>
                                                    <ListItem>
                                                    <ListItemText primary={f.food+": "+f.buc+" buc"} secondary={f.price}/>
                                                    </ListItem>                       
                                                    <Divider />
                                                </div>
                                        )
                                    }
                                </div>
                                </List>
                            
                        </Grid>
                        </div>
                        <Grid
                            container
                            spacing={0}
                            direction="column"
                            alignItems="center"
                            justify="center"
                            style={{ minHeight: '40vh' }}
                            >
                            <RaisedButton label="Order" style={style} onClick={(event) => this.handleSave()}/>
                            <RaisedButton label="Back" style={style} onClick={(event) => this.handleBack()}/>
                        </Grid>
                    </div>
                </MuiThemeProvider>
            </div> 
        )
    }
}

const style = {
    margin: 15,
   };

export default Cart

