import React, { Component } from 'react';
import './App.css';
import Loginscreen from './Loginscreen';

class App extends Component {
  constructor(props){
    super(props);
    this.state={
      loginPage:[],
      home:[],
      menu:[],
      food:[],
      account:[],
      order:[],
      allorders:[],
      admin:[],
      restaurantId:-1,
      accountId:-1,
      menuId:-1,
      adminId:-1,
      allordersid:-1,
      orders:[],
      messages:[],
      orderlist:[]
    }
  }
  componentWillMount(){
    var loginPage =[];
    loginPage.push(<Loginscreen parentContext={this}/>);
    this.setState({
                  loginPage:loginPage
                    })
  }
  render() {
    return (
        <div className="App">
          {this.state.loginPage}
          {this.state.home}
          {this.state.menu}
          {this.state.account}
          {this.state.food}
          {this.state.order}
          {this.state.allorders}
          {this.state.admin}
          {this.state.orderlist}
        </div>
    );
  }
}
const style = {
  margin: 15,
};
export default App;