
import React, { Component } from 'react';
import * as Colors from 'material-ui/styles/colors';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import RaisedButton from 'material-ui/RaisedButton';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import IconButton from '@material-ui/core/IconButton';
import Login from "./App";
import Grid from '@material-ui/core/Grid';
import Menus from "./Menus";
import axios from 'axios'
import Account from "./Account"
import Order from "./Cart"
import AllOrders from './AllOrders'


const muiTheme = getMuiTheme({
    palette: {
      textColor: Colors.white,
      primary1Color: Colors.white,
      primary2Color: Colors.cyan500,
      accent1Color: Colors.redA200,
      pickerHeaderColor: Colors.white,
      alternateTextColor: Colors.cyan500,
    },
    
  });

class Restaurants extends Component {

      constructor(props) {
            super(props)
            this.state = {
                restaurants: [],
                message: null,
                auth: true,
                anchorEl: null,
            }
            this.refreshRestaurants = this.refreshRestaurants.bind(this)
      }
    
    
      handleMenu = event => {
        this.setState({ anchorEl: event.currentTarget });
      };
    
      handleProfile = () => {
        var self = this;
        var account=[];
        account.push(<Account appContext={self.props.appContext} />)
        self.props.appContext.setState({home:[],account:account})
      };

      handleOrders = () => {
        var self = this;
        var orders=[];
        orders.push(<AllOrders appContext={self.props.appContext} />)
        self.props.appContext.setState({allorders:orders,home:[]})
      };

      handleLogout = () => {
        var self = this;
        var login=[];
        axios.post('http://localhost:8080/spring_demo_war/order/clearcart');
        login.push(<Login appContext={self.props.appContext} />)
        self.props.appContext.setState({loginPage:login,home:[]})
      };

      handleClick(event,id){
        var self = this;
        var menu=[];
        menu.push(<Menus appContext={self.props.appContext}/>)
        self.props.appContext.setState({home:[],menu:menu,restaurantId:id})
      }

      handleCart(){
        var self = this;
        var order=[];
        order.push(<Order appContext={self.props.appContext}/>)
        self.props.appContext.setState({order:order,home:[]})
      } 



    componentDidMount() {
        this.refreshRestaurants();
    }

    refreshRestaurants() {
        axios.get('http://localhost:8080/spring_demo_war/order/restaurants')
            .then(
                response => {
                    console.log(response);
                    this.setState({ restaurants: response.data })
                }
            )
    }
    render() {
        window.history.pushState("object or string", "Title", "/restaurnats");
        const { auth, anchorEl } = this.state;
        const open = Boolean(anchorEl);
        const { classes } = this.props;
        return (
            <div>
                <MuiThemeProvider muiTheme={muiTheme}>
                    <div>
                        <AppBar
                            style={{ backgroundColor: "#00bcd4"}}
                            position="static"
                        >
                            <Toolbar>
                                <Typography variant="h6" color="inherit">
                                Foodistic
                                </Typography>
                                <div>
                                    <IconButton
                                        aria-owns={open ? 'menu-appbar' : undefined}
                                        aria-haspopup="true"
                                        onClick={this.handleMenu}
                                        color="inherit"
                                        
                                        >
                                        <AccountCircle/>
                                    </IconButton>
                                    <Menu
                                        id="menu-appbar"
                                        anchorEl={anchorEl}
                                        anchorOrigin={{
                                            vertical: 'top',
                                            horizontal: 'right',
                                        }}
                                        transformOrigin={{
                                            vertical: 'top',
                                            horizontal: 'right',
                                        }}
                                        open={Boolean(this.state.anchorEl)}
                                        onClose={() => this.setState({ anchorEl: null })}
                                        >
                                        <MenuItem onClick={this.handleProfile}>Profile</MenuItem>
                                        <MenuItem onClick={this.handleOrders}>My Orders</MenuItem>
                                        <MenuItem onClick={this.handleLogout}>Logout</MenuItem>
                                    </Menu>
                                </div>
                                <RaisedButton label="Cart" style={style} onClick={(event) => this.handleCart()}/>
                            </Toolbar>
                        </AppBar>
                        <Grid
                            container
                            spacing={0}
                            direction="column"
                            alignItems="center"
                            justify="center"
                            style={{ minHeight: '50vh' }}
                            >
                            <h2>Restaurants</h2>
                            <div>
                                {
                                    this.state.restaurants.map(
                                        restaurant =>
                                        <RaisedButton label={restaurant.name} style={style} onClick={(event) => this.handleClick(event,restaurant.id)}/>
                                    )
                                }
                            </div>
                        </Grid>
                    </div>
                </MuiThemeProvider>
            </div> 
        )
    }
}

const style = {
    margin: 15,
   };

export default Restaurants

