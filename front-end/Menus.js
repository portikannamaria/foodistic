
import React, { Component } from 'react';
import * as Colors from 'material-ui/styles/colors';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import RaisedButton from 'material-ui/RaisedButton';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Grid from '@material-ui/core/Grid';
import Home from "./Restaurants";
import axios from 'axios'
import Food from "./Foods"
import Order from './Cart'


const muiTheme = getMuiTheme({
    palette: {
      textColor: Colors.white,
      primary1Color: Colors.white,
      primary2Color: Colors.cyan500,
      accent1Color: Colors.redA200,
      pickerHeaderColor: Colors.white,
      alternateTextColor: Colors.cyan500,
    },
    
  });

class Menus extends Component {

    constructor(props) {
        super(props)
        this.state = {
            menu: [],
            message: null,
            auth: true,
            anchorEl: null,
        }
        this.refreshMenu = this.refreshMenu.bind(this)
    }
    

      handleClick(event,id){
        var self = this;
        var food=[];
        food.push(<Food appContext={self.props.appContext}/>)
        self.props.appContext.setState({food:food,menu:[],menuId:id})
      }

      handleBack(){
        var self = this;
        var home=[];
        home.push(<Home appContext={self.props.appContext}/>)
        self.props.appContext.setState({home:home,menu:[]})
      }

      handleCart(){
        var self = this;
        var order=[];
        order.push(<Order appContext={self.props.appContext}/>)
        self.props.appContext.setState({order:order,menu:[]})
      } 


    componentDidMount() {
        this.refreshMenu();
    }

    refreshMenu(){
        var self = this;
         axios.get('http://localhost:8080/spring_demo_war/order/menus', {
            params: {
             id:self.props.appContext.state.restaurantId
             }})
        .then(
                response => {
                    console.log(response);
                    this.setState({ menu: response.data })
                }
            )
    }
    render() {
        window.history.pushState("object or string", "Title", "/menus");
        const { anchorEl } = this.state;
        const open = Boolean(anchorEl);
        return (
            <div>
                <MuiThemeProvider muiTheme={muiTheme}>
                    <div>
                        <AppBar
                            style={{ backgroundColor: "#00bcd4"}}
                            position="static"
                        >
                            <Toolbar>
                                <Typography variant="h6" color="inherit">
                                Foodistic
                                </Typography>
                                <RaisedButton label="Cart" style={style} onClick={(event) => this.handleCart()}/>
                            </Toolbar>
                        </AppBar>
                        <Grid
                            container
                            spacing={0}
                            direction="column"
                            alignItems="center"
                            justify="center"
                            style={{ minHeight: '40vh' }}
                            >
                            <h2>Menu</h2>
                            <div>
                                {
                                    this.state.menu.map(
                                        m =>
                                        <RaisedButton label={m.name} style={style} onClick={(event) => this.handleClick(event,m.id)}/>
                                    )
                                }
                            </div>
                        </Grid>
                        <Grid
                            container
                            spacing={0}
                            direction="column"
                            alignItems="center"
                            justify="center"
                            style={{ minHeight: '40vh' }}
                            >
                            <RaisedButton label="Back" style={style} onClick={(event) => this.handleBack()}/>
                        </Grid>
                    </div>
                </MuiThemeProvider>
            </div> 
        )
    }
}

const style = {
    margin: 15,
   };

export default Menus

